FROM nginx:1.23.3

EXPOSE 80

RUN mkdir -p /app/www
WORKDIR /app
COPY www ./www
COPY config /etc/nginx