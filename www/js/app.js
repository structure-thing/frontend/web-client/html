import { NodeTreeView } from './modules/tree.js'
import { Connector, Node } from './modules/connector.js'

$(document).ready(async function(){
    const treeView = new NodeTreeView($('#node-container-main'))

    let lastRootNode = localStorage.lastRootNode
    if(lastRootNode){
        lastRootNode = JSON.parse(lastRootNode)
        lastRootNode = Object.assign(new Node(), lastRootNode)
        function forChild(parent){
            for(const index in parent.children){
                parent.children[index] = Object.assign(new Node(), parent.children[index])
                forChild(parent.children[index])
            }
        }
        forChild(lastRootNode)
        await treeView.setRootNode(lastRootNode)

        treeView.setExpandedNodeIds([lastRootNode.getId()])
    }

    const self = this

    const data = function (){
        const search = document.location.search.replace('?', '')
        if(search == ''){
            return {}
        }
        const data = search.split('&').map(kv => kv.split('='))
        return Object.fromEntries(data)
    }()

    let node_id = data.node_id

    const connector = new Connector(node_id, 'api', data.token)
    let user = undefined
    try{
        user = await connector.me_get()
    }catch(e){
        console.log(e)
        let newLocation = '/login'
        if(data.node_id) {
            newLocation += `?node_id=${data.node_id}`
        }
        document.location = newLocation
        return
    }
    if(!node_id){
        const rootNode = user.rootNode
        data.node_id = rootNode.id
        document.location = `${location.protocol}//${location.host}${location.pathname}?${new URLSearchParams(data)}`

        return
    }

    connector.csrf_token = user.csrf_token
    const permissions = user.permissions

    treeView.setPermissions(permissions)

    treeView.onnodeattributevalueupdate = async function(...args){
        connector.node_attribute_value_set(...args)
    }

    const permission_children = (permissions.children == true)

    async function reloadRootNode(){
        $('#loading-popup').show()
        const root_node = await connector.root_node_get(permission_children, permissions.attributes_read == true)

        function getLocalNodeField(node, field){
            node[field] = localStorage[`node_${node.getId()}_${field}`]

            for(const child of node.children ?? []){
                getLocalNodeField(child, field)
            }
        }
        getLocalNodeField(root_node, 'sort')
        getLocalNodeField(root_node, 'filter')

        localStorage.lastRootNode = JSON.stringify(root_node)

        const expandedNodeIds = treeView.getExpandedNodeIds()

        await treeView.setRootNode(root_node)
        if((!lastRootNode) || (root_node.getId() != lastRootNode.getId())){
            treeView.setExpandedNodeIds([root_node.getId()])
        }else{
            if(expandedNodeIds.length > 0){
                treeView.setExpandedNodeIds(expandedNodeIds)
            }else{
                treeView.setExpandedNodeIds([node_id])
            }
        }

        document.title = root_node.getName()
        $('#loading-popup').hide()
    }

    await reloadRootNode()

    if(data.focused_node) {
        treeView.focusNode(data.focused_node)
        treeView.expandNode(data.focused_node)
    }

    treeView.onnodeupdate = async function(type, node){
        if(type == 'edit'){
            const id = node.getId()
            delete node.getId()
            function storeLocalNodeField(field){
                if(node[field]){
                    localStorage[`node_${id}_${field}`] = node[field]
                }else{
                    localStorage.removeItem(`node_${id}_${field}`)
                }
            }
            storeLocalNodeField('sort')
            storeLocalNodeField('filter')

            await connector.node_update(id, node)
            return
        }
        if(type == 'delete'){
            await connector.node_delete(node.getId())
            return
        }
        if(type == 'create'){
            await connector.node_create(node)
            return
        }
    }

    treeView.loadaccounts = async function(node_id){
        return await connector.getNodeAccounts(node_id)
    }

    treeView.loadaccounttoken = async function(...args){
        return await connector.getNodeAccountToken(...args)
    }

    treeView.onaccountupdate = async function(type, node, ...args){
        if(type == 'create'){
            connector.createNodeAccount(node.getId(), ...args)
        }else if(type == 'delete'){
            connector.node_accounts_delete(node.getId(), ...args)
        }
    }

    connector.onattributeschange = async function (node_id, attributes) {
        let node_error = false
        for (const attribute of attributes) {
            const valid = attribute.valid
            node_error |= (!valid)
            await treeView.setNodeAttribute(node_id, attribute, true, true)
            treeView.animateAttribute(node_id, attribute.id, valid)
        }
        treeView.setNodeValidColor(node_id, !node_error)
        if(!node_error){
            return
        }
        treeView.focusNode(node_id, false)
    }

    function nodeUpdateHandler(type){
        return async function(){
            const expandedIds = treeView.getExpandedNodeIds()
            await reloadRootNode()
            treeView.setExpandedNodeIds(expandedIds)

            treeView.setLoadingDialogVisible(false)
            if(type == 'delete'){
                return
            }
            // treeView.focusNode(this.getId(), false)
            treeView.animateNode(this.getId())
        }
    }

    connector.onnodechange = nodeUpdateHandler('change')
    connector.onnodecreate = nodeUpdateHandler('create')
    connector.onnodedelete = nodeUpdateHandler('delete')
    connector.events_connect(permission_children)
})
