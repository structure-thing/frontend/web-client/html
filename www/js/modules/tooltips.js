export const tooltips = {
    '#dialog-attribute-edit': {
        '#attribute-id': 'This identifies an attribute in the API. This field is mostly used to change the value of the attribute through the API or retreive the value of the attribute through the API.',
        '#attribute-name': 'This field is the user friendly name for the attribute in the UI. It has no relevance for the attribute API',
        'div[data-attribute-field-category="filter"] input': 'This jsonata expression should yield true or false. Whenever a new value for the attribute is uploaded, the new value firstly undergoes this expression. Only when the result is true, the API will accept the new value. This can be used for type/range checking, or to only allow certain values.',
        'div[data-attribute-field-category="validation"] input': 'Before an attribute update is broadcasted to clients, the new value undergoes this jsonata expression. If this expression yields true, the attribute value will be highlighted green in the user interface. If it yields false, the attribute and whole node will be red. In the future, that may yield a user notification.',
        'div[data-attribute-field-category="transformation"] input': 'When a new attribute value is uploaded, it undergoes this jsonata expression before being stored in the attribute database. You could use this to convert types, do a range mapping, a classification or other transformations. The output of this expression will finally be sent to the attribute database.',
        'div[data-attribute-field-category="transformation_display"] input': 'This expression takes effect when an attribute value is read from the attribute database, for example when the user interface is reloaded or a new value is broadcastet. You can use this expression to beautify the raw attribute value, for example convert 50 to "50%". You can also build time-based value rules using this rule.',
        'div[data-attribute-field-category="custom"] input': 'This expression is just free text without any technical meaning for the platform. It may be interpreted by third party systems.'
    },
    '#dialog-edit-node': {
        '#input-node-name': 'This gives the node a nice name in the user interface. While the node id should primarily be used to address the node or its attributes, the name field can also be used to update multiple nodes or their attributes through the filter "filter.name".',
        '#input-node-description': 'This gives the node a description in the user interface. While the node id should primarily be used to address the node or its attributes, the name field can also be used to update multiple nodes or their attributes through "filter.description".',
        '#input-node-sort': 'This expression is applied to each direct child node and should return a comparable value like a string or an int. The resulting value will be used to sort all direct children.'
    },
    '#dialog-account-edit': {
        '#account-type-select': '<b class="tooltip-header">User invitation</b>has to be sent to a new user to create a new account.<b class="tooltip-header">Authenticated link</b>Allows for access to a node (and possibly its children) without having to create an account<b class="tooltip-header">API token</b>generates a plain Bearer token used to authenticate against the API',
        '#account-label': 'Gives this account in the user interface a nice label, no technical meaning.',
        '#permission-children': 'Allows the user to view, create, modify and delete children nodes. If this permission is absent, the user only can only see his home node, no children.',
        '#permission-node-read': 'Allows the user to read the node name, description and attribute metadata, not the attribute values.',
        '#permission-node-write': 'Allows the user to modify the node name, description and attribute metadata, not the attribute values.',
        '#permission-attributes-read': 'Allows the user to read / subscribe to attribute values, but not to change them. Also doesn\'t give any access to node data.',
        '#permission-attributes-write': 'Allows the user to write attribute values. Also doesn\'t give any access to node data.',
        '#permission-accounts': 'Allows the user to create additional accounts / invitations for accessible nodes.',
    },
}