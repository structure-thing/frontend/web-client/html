// would prefer import with assert:json here, but firefoy doesn't support that...
const schema_nodes = await fetch('/documentation/schemas/nodes.json').then(response => response.json())
const schema_users = await fetch('/documentation/schemas/users.json').then(response => response.json())

export class Node{
    constructor(id, parentPath, name, description, attributes, cloneOf, attributesTemplate){
        this.id = id
        this.parentPath = parentPath
        this.name = name
        this.description = description
        this.attributes = attributes
        this.children = []
        this.cloneOf = cloneOf
        this.attributesTemplate = attributesTemplate
        this.filter = undefined
        this.sort = undefined
    }

    addChild(child){
        this.children.push(child)
    }

    getChildren(){
        return this.children
    }

    getId(){
        return this.id
    }

    setId(id){
        this.id = id
    }

    getParentPath(){
        return this.parentPath
    }

    setParentPath(parentPath){
        this.parentPath = parentPath
    }

    getName(){
        return this.name
    }

    setName(name){
        this.name = name
    }

    getDescription(){
        return this.description
    }

    setDescription(description){
        this.description = description
    }

    getAttributes(){
        return this.attributes
    }

    getAttributesTemplate(){
        return this.attributesTemplate
    }

    setAttributes(attributes){
        this.attributes = attributes
    }

    getClonedId(){
        return this.cloneOf
    }

    getParentNodeId(){
        return this.parentPath.slice(-24)
    }

    getSortExpression(){
        return this.sort
    }

    setSortExpression(expression){
        this.sort = expression
    }

    getFilterExpression(){
        return this.filter
    }

    setFilterExpression(expression){
        this.filter = expression
    }

    traverseTree(func, node=this){
        func(node)
        for(const child of node.children){
            this.traverseTree(func, child)
        }
    }
}

export class Connector {
    constructor(base_node_id, base_uri = 'api', token) {
        this.base_node_id = base_node_id
        this.base_uri = base_uri
        this.onattributeschange = undefined
        this.onnodechange = undefined
        this.onnodedelete = undefined
        this.onnodecreate = undefined
        this.websocket = undefined
        this.token = token
    }
    
    stringify_filter(object, schema){
        object = this.object_filter_schema(object, schema)
        return JSON.stringify(object)
    }

    object_filter_schema(object, schema){
        if(schema.bsonType == 'object'){
            const object_new = {}
            for(const key of Object.keys(schema.properties ?? {})){
                const target_object = object[key]
                if(target_object == undefined){
                    continue
                }
                object_new[key] = this.object_filter_schema(target_object, schema.properties[key])
            }
            return object_new
        }
        if(schema.bsonType == 'array'){
            return object.map(item => this.object_filter_schema(item, schema.items))
        }
        
        return object
    }

    node_build_children_tree(nodes){
        let rootNode = new Node()

        // convert to internal object
        for(const index in nodes){
            const node = nodes[index]
            const newNode = nodes[index] = new Node(node._id, node.parentPath, node.name, node.description, node.attributes, node.cloneOf, node.attributesTemplate)
        }

        const nodeDict = {}

        for(const node of nodes){
            nodeDict[node.getId()] = node
        }

        for(const node of nodes){
            (function(){
                if(node.getClonedId() == undefined) return
                const clonedNode = nodeDict[node.getClonedId()] ?? new Node(
                    undefined, 
                    undefined, 
                    'Clone of unknown original', 
                    'The original is either not accessible or deleted'
                )
                node.setName(clonedNode.getName())
                node.setDescription(clonedNode.getDescription())
                node.setAttributes(clonedNode.getAttributes())
            })();
        }

        for(const node of nodes){
            const parentPath = node.getParentPath()

            function traverseNode(rootNode, parts){
                const currentParent = parts[0]

                let intermediateNode = rootNode.getChildren().find(candidate => candidate.getId() == currentParent)

                if(intermediateNode == undefined){
                    intermediateNode = new Node(
                        currentParent
                    )
                    let parentPath = ''
                    if(rootNode.getParentPath()){
                        parentPath += rootNode.getParentPath()
                    }
                    if(rootNode.getId() != undefined){
                        parentPath += '/' + rootNode.getId()
                    }
                    if(parentPath){
                        intermediateNode.setParentPath(parentPath)
                    }
                    rootNode.addChild(intermediateNode)
                }
                if(parts.length == 1){
                    const children = intermediateNode.children
                    Object.assign(intermediateNode, node)
                    intermediateNode.children = children
                    return
                }

                traverseNode(intermediateNode, parts.slice(1))
            }

            let pathParts = []
            if(parentPath != ''){
                pathParts = parentPath.substring(1).split('/')
            }
            pathParts.push(node.getId())
            pathParts = pathParts.slice(
                pathParts.indexOf(this.base_node_id)
            )
            traverseNode(rootNode, pathParts)
        }

        if(rootNode.length == 0){
            return undefined
        }

        return rootNode.getChildren()[0]
    }

    async send_request(url, options){
        if(options == undefined){
            options = {}
        }
        if(options.headers == undefined){
            options.headers = {}
        }
        Object.assign(options.headers, {
            'X-CSRF-TOKEN': this.csrf_token
        })
        if(this.token != undefined){
            options.headers['Authorization'] = `Bearer ${this.token}`
        }
        const response = await fetch(url, options)
        if(!response.ok){
            throw await response.text()
        }
        return response
    }

    async node_accounts_delete(node_id, account_id){
        const url = `${this.base_uri}/nodes/${node_id}/accounts/${account_id}`

        const result = await this.send_request(url, {
            method: 'DELETE'
        })
        return await result.json()
    }

    async getNodeAccounts(node_id){
        const url = `${this.base_uri}/nodes/${node_id}/accounts`

        const result = await this.send_request(url)
        return await result.json()
    }

    async getNodeAccountToken(node_id, account_id){
        const url = `${this.base_uri}/nodes/${node_id}/accounts/${account_id}/token`

        const result = await this.send_request(url)
        return await result.text()
    }

    async createNodeAccount(node_id, type, label, permissions){
        const url = `${this.base_uri}/nodes/${node_id}/accounts`
        const payload = {
            type: type,
            label: label,
            permissions: permissions
        }
        const result = await this.send_request(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: this.stringify_filter(payload, schema_users)
        })
    }

    async node_get(node_id, include_children = false, include_attributes = false) {
        const params = [`id=${node_id}`]
        if (include_children) {
            params.push('include=children')
        }
        if (include_attributes) {
            params.push('include=attributes')
        }
        let url = `${this.base_uri}/nodes?${params.join('&')}`
        const result = await this.send_request(url)
        const resultList = await result.json()
        return this.node_build_children_tree(resultList)
    }

    async root_node_get(include_children = false, include_attributes = false) {
        return await this.node_get(this.base_node_id, include_children, include_attributes)
    }

    async node_create(node) {
        const parentId = node.getParentNodeId()
        const url = `${this.base_uri}/nodes/${parentId}/children`
        const result = await this.send_request(url, {
            method: 'POST',
            body: this.stringify_filter(node, schema_nodes),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return await result.json()
    }

    async node_update(node_id, node_data) {
        let url = `${this.base_uri}/nodes/${node_id}`
        const result = await this.send_request(url, {
            method: 'PATCH',
            body: this.stringify_filter(node_data, schema_nodes),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return await result.json()
    }

    async node_delete(node_id) {
        let url = `${this.base_uri}/nodes/${node_id}`
        const result = await this.send_request(url, {
            method: 'DELETE'
        })
        return await result.json()
    }

    async node_attribute_value_set(node_id, attribute_id, value) {
        let url = `${this.base_uri}/nodes/${node_id}/attributes/${attribute_id}`
        const result = await this.send_request(url, {
            method: 'POST',
            body: value
        })
    }

    async me_get(){
        let url = `${this.base_uri}/auth/me`
        const result = await this.send_request(url)
        return await result.json()
    }

    convertLegacyNode(legacyNode){
        return new Node(
            legacyNode._id,
            legacyNode.parentPath,
            legacyNode.name,
            legacyNode.description,
            legacyNode.attributes,
            legacyNode.cloneOf,
            legacyNode.attributesTemplate
        )
    }

    events_connect(include_children) {
        const self = this

        let url = `${location.protocol.replace('http', 'ws')}//${location.host}`
        const args = {
            id: this.base_node_id
        }
        if(include_children){
            args.include = 'children'
        }
        if(self.token != undefined){
            args.token = self.token
        }

        const socket = io(url, {
            query: new URLSearchParams(args).toString(),
            path: `/${this.base_uri}/socket.io`,
            extraHeaders: {
                'x-csrf-token': this.csrf_token
            }
        })

        socket.on('node.update', async message => {
            if (message.type == 'change') {
                if (message.subject == 'node.attributes' && typeof (self.onattributeschange) == 'function') {
                    const node = self.convertLegacyNode(message.data.node)
                    await self.onattributeschange(node.getId(), node.attributes)
                    return
                }
                if (message.subject == 'node' && typeof (self.onnodechange) == 'function') {
                    const node = self.convertLegacyNode(message.data.node)
                    await self.onnodechange.call(node)
                    return
                }
            }
            if (message.type == 'delete'){
                if (message.subject == 'node' && typeof (self.onnodedelete) == 'function') {
                    const node = self.convertLegacyNode(message.data.node)
                    await self.onnodedelete.call(node)
                    return
                }
            }
            if (message.type == 'create'){
                if (message.subject == 'node' && typeof (self.onnodecreate) == 'function') {
                    const node = self.convertLegacyNode(message.data.node)
                    await self.onnodecreate.call(node)
                    return
                }
            }
        })
    }
}