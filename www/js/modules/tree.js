import { tooltips } from './tooltips.js'
import { Node } from './connector.js'

export class NodeTreeView {
    constructor(parent_container, permissions) {
        this.parent_container = parent_container
        this.node_dict = {}
        this.onnodeupdate = undefined
        this.onaccountupdate = undefined
        this.onnodeattributevalueupdate = undefined
        this.node_template = $('#node-template').contents()
        this.attribute_row_template = $('#attribute-row-template').contents()

        if(permissions){
            this.setPermissions(permissions)
        }

        this.copied_node = null
        this.node_is_cut = false

        $('#dialog-waiting-refresh').dialog({
            autoOpen: false
        })

        $.notify.defaults({
            className: 'success'
        })

        this.attachTooltips()
        this.attachContextMenu(parent_container)
    }

    setPermissions(permissions) {
        this.permission_node_read = permissions['node_read'] == true
        this.permission_node_write = permissions['node_write'] == true
        this.permission_attributes_read = permissions['attributes_read'] == true
        this.permission_attributes_write = permissions['attributes_write'] == true
        this.permission_children = permissions['children'] == true
        this.permission_accounts = permissions['accounts'] == true
    }

    attachTooltips(selector='', value=tooltips) {
        if(typeof(value) == 'object'){
            for(const key of Object.keys(value)){
                this.attachTooltips(`${selector} ${key}`, value[key])
            }
            return
        }
        const element = $(selector)
        element.tooltip('dispose')
        element.tooltip({title: value, html: true})
    }

    async editObjectDialog(dialogSelector, objectToEdit){
        const isNew = objectToEdit == undefined
        objectToEdit ||= {}

        let promise = undefined
        const result = new Promise(resolve => {
            promise = resolve
        })

        const dialog = $(dialogSelector)

        const inputs = $('*[data-property-key]', dialog)

        function getObjectByPath(parentObject, parts){
            const currentIndex = parts[0]
            if(parts.length == 1){
                return parentObject[currentIndex]
            }
            if(parentObject[currentIndex] == undefined){
                return undefined
            }
            return getObjectByPath(parentObject[currentIndex], parts.slice(1))
        }

        for(let input of inputs){
            input = $(input)
            const is_checkbox = input.prop('type') == 'checkbox'
            const is_select = input.is('select')
            if(isNew){
                if(is_checkbox){
                    input.prop('checked', false)
                }else if(!is_select){
                    input.val('')
                }
                continue
            }
            const key = input.attr('data-property-key')
            const parts = key.split('.')
            const value = getObjectByPath(objectToEdit, parts)
            if(is_checkbox){
                input.prop('checked', value)
            }else{
                input.val(value)
            }
        }

        dialog.dialog({
            buttons: [{
                text: 'OK',
                class: 'btn btn-primary',
                click: function(){
                    dialog.dialog('close')
                    const inputs = $('*[data-property-key]', dialog)
                    for(let input of inputs){
                        input = $(input)
                        let value = input.val()
                        if(input.prop('type') == 'checkbox'){
                            value = input.is(':checked')
                        }
                        if(input.prop('type') == 'number'){
                            value = Number(value)
                        }
                        const key = input.attr('data-property-key')
                        function setObjectByPath(parentObject, parts, value){
                            const currentIndex = parts[0]
                            if(parts.length == 1){
                                parentObject[currentIndex] = value
                                return
                            }
                            if(parentObject[currentIndex] == undefined){
                                parentObject[currentIndex] = {}
                            }
                            setObjectByPath(parentObject[currentIndex], parts.slice(1), value)
                        }
                        const parts = key.split('.')
                        setObjectByPath(objectToEdit, parts, value)
                    }
                    promise(objectToEdit)
                }
            }]
        })

        return result
    }

    setLoadingDialogVisible(visible) {
        $('#dialog-waiting-refresh').dialog(visible ? 'open' : 'close')
        if(visible){
            $('div[role="dialog"]').css({position: 'fixed'})
        }
    }

    async showAccountsDialog(node){
        const dialog = $('#dialog-accounts')

        async function loadAccountsForNode(node){
            const accounts = await this.loadaccounts(node.getId())
            const template = $('#account-template', dialog)
            const container = $('#accounts-container', dialog)
            container.empty()
            for(const account of accounts){
                const line = template.clone()
                line.removeClass('d-none')

                const clipboard = $('.ui-icon-clipboard', line)
                clipboard.show()

                $('#account-username', line).text('')
                $('#account-type', line).text(account.type)
                $('#account-label', line).text(account.label)
                $('.ui-icon-trash', line).click(async () => {
                    await this.onaccountupdate('delete', node, account._id)
                    setTimeout(loadAccountsForNode.bind(this, node), 500)
                })
                const permissionsContainer = $('#account-permissions', line)
                
                permissionsContainer.empty()
                for(const permission in account.permissions){
                    if(account.permissions[permission] != true){
                        continue
                    }
                    const permissionText = $('<div>')
                    permissionText.addClass('user-permission')
                    permissionText.text(permission)
                    permissionsContainer.append(permissionText)
                }

                if(account.type == 'invitation'){
                    clipboard.click(async () => {
                        const base = `${location.protocol}//${location.hostname}`
                        const url = `${base}/register?invitation=${account.username}`
                        console.log(url)
                        try{
                            await navigator.clipboard.writeText(url)
                            dialog.notify('invitation link copied to clipboard')
                        }catch(e){
                            console.error(e)
                            dialog.notify(`error copying invitation link to clipboard: ${url}`, 'error')
                        }
                    })
                }else if(account.type == 'api-token'){
                    clipboard.click(async () => {
                        const token = await this.loadaccounttoken(node.getId(), account._id)
                        console.log(token)
                        try{
                            await navigator.clipboard.writeText(token)
                            dialog.notify('token copied to clipboard')
                        }catch(e){
                            console.error(e)
                            dialog.notify(`error copying token to clipboard: ${token}`, 'error')
                        }
                    })
                }else if(account.type == 'link'){
                    clipboard.click(async () => {
                        const data = {
                            node_id: node.getId(),
                            token: await this.loadaccounttoken(node.getId(), account._id)
                        }
                        const url = `${location.protocol}//${location.host}?${new URLSearchParams(data)}`
                        console.log(url)
                        try{
                            await navigator.clipboard.writeText(url)
                            dialog.notify('URL copied to clipboard')
                        }catch(e){
                            console.error(e)
                            dialog.notify(`error copying URL to clipboard: ${url}`, 'error')
                        }
                    })
                }else if(account.type == 'user'){
                    clipboard.hide()
                    $('#account-username', line).text(account.username)
                }
                container.append(line)
            }
        }

        await loadAccountsForNode.call(this, node)

        $('#button-account-add').click(async () => {
            await this.editObjectDialog('#dialog-account-edit')
                .then(async (permissions) => {
                    const label = permissions.label
                    delete permissions.label
                    const type = permissions.type
                    delete permissions.type
                    if(typeof(this.onaccountupdate) == 'function'){
                        await this.onaccountupdate('create', node, type, label, permissions)
                        setTimeout(loadAccountsForNode.bind(this, node), 500)
                    }
                })
        })
        dialog.dialog()
    }

    async runWithDialog(func, ...params){
        this.setLoadingDialogVisible(true)
        try{
            return await func(...params)
        }catch(e){
            console.error(e)
            this.setLoadingDialogVisible(false)
            $('#dialog-error').dialog()
            $('#dialog-error #text-error').text(e)
        }finally{
        }
    }

    async node_paste_deep_copy(node_into){
        if(this.node_is_cut){
            this.copied_node.setParentPath(`${node_into.getParentPath()}/${node_into.getId()}`)
            await this.runWithDialog(this.onnodeupdate, 'edit', this.copied_node)
            this.copied_node = null
            return
        }
        const source = this.copied_node
        const deep_copy = new Node(
            undefined,
            `${node_into.getParentPath()}/${node_into.getId()}`,
            source.getName() == undefined ? undefined : `${source.getName()} copy`,
            source.getDescription(),
            source.getAttributes(),
            source.getClonedId(),
            source.getAttributesTemplate()
        )
        await this.runWithDialog(this.onnodeupdate, 'create', deep_copy)
    }

    async node_paste_template(node_into){
        const deep_copy = new Node(
            undefined,
            `${node_into.getParentPath()}/${node_into.getId()}`,
            this.copied_node.getName() == undefined ? undefined : `${this.copied_node.getName()} copy`,
            this.copied_node.getDescription(),
            undefined,
            undefined,
            this.copied_node.getId()
        )
        await this.runWithDialog(this.onnodeupdate, 'create', deep_copy)
    }

    async node_paste_clone(node_into){
        const clone = new Node(
            undefined,
            `${node_into.getParentPath()}/${node_into.getId()}`,
            undefined,
            undefined,
            undefined,
            this.copied_node.getId()
        )
        await this.runWithDialog(this.onnodeupdate, 'create', clone)
    }

    async filterNodeChildren(node){
        async function checkfilter(node, expression){
            // show if no filter to check
            if(!expression){
                node.filterShown = true
            }

            if(node.getFilterExpression()){
                if(expression){
                    console.log('expression disabled for node ...')
                    node.filterState = 'overridden'
                }else{
                    try{
                        expression = jsonata(node.getFilterExpression())
                        node.filterState = 'applied'
                    }catch(e){
                        console.error(e)
                        node.filterShown = true
                        node.filterState = 'error'
                    }
                }
            }

            for(const child of node.children){
                try{
                    node.filterShown |= await checkfilter(child, expression)
                }catch(e){
                    console.error(e)
                    child.filterShown = true
                    child.filterState = node.filterState = 'error'
                }
            }

            // no need to recalculate if one of children apply
            if(node.filterShown){
                return true
            }

            return node.filterShown = await this.performJsonataCall(expression, node)
        }

        await checkfilter(node)

        function applyNodeFilter(node){
            if(!node.filterShown){
                const nodeElement = $(`[data-node-id=${node.getId()}]`)
                nodeElement.hide()
            }
            if(node.filterState == 'applied'){
                // why is this line duplicated here? performance...
                const nodeElement = $(`[data-node-id=${node.getId()}]`)
                $('> div > div > img#filter-ok', nodeElement).show()
            }else if(node.filterState){
                const nodeElement = $(`[data-node-id=${node.getId()}]`)
                const filterError = $('> div > div > img#filter-error', nodeElement)
                filterError.show()
                filterError.attr('title', {
                    'overridden': 'Higher order node filter applied, this one is disabled',
                    'error': 'Error in filter expression'
                }[node.filterState])
            }

            for(const child of node.children){
                applyNodeFilter(child)
            }
        }

        applyNodeFilter(node)
    }

    async performJsonataCall(expression, node){
        const oldAttributes = node.attributes
        node.attributes = {}
        for(const attribute of oldAttributes){
            // should we use display value here?
            node.attributes[attribute.id] = attribute.value
        }

        function aggregateAttributes(id, node, attributes, result=[]){
            for(const attribute of attributes ?? []){
                if(attribute.id == id){
                    result.push(attribute.value)
                }
            }
            for(const child of node.children ?? []){
                aggregateAttributes(id, child, child.attributes, result)
            }
            return result
        }

        try{
            return await expression.evaluate(node, {
                aggregateAttributes: (id) => aggregateAttributes(id, node, oldAttributes)
            })
        }finally{
            node.attributes = oldAttributes
        }
    }

    async sortNodeChildren(node){
        await this.sortSingleNodeChildren(node)
        for(const child of node.children ?? []){
            await this.sortNodeChildren(child)
        }
    }

    async sortSingleNodeChildren(node){
        if(!node.getSortExpression()){
            return
        }
        const nodeElement = $(`[data-node-id=${node.getId()}]`)

        let sortedChildren = node.children

        const expression = jsonata(node.getSortExpression())

        try{
            await Promise.all(sortedChildren.map(async child => child.sort_cardinality = await this.performJsonataCall(expression, child)))
        } catch(e){
            console.error(e)
            $('> div > div > img#sort-error', nodeElement).show()
            return
        }

        sortedChildren = sortedChildren.toSorted((a, b) => {
            if (a.sort_cardinality > b.sort_cardinality){
                return 1
            }
            if (a.sort_cardinality < b.sort_cardinality){
                return -1
            }
            return 0
        })

        $('> div > div > img#sort-ok', nodeElement).show()

        const container = $(`> div#node-children.collapse.node-children-container`, nodeElement)

        const oldChildren = container.children()

        const childrenIndex = {}

        for(const oldChild of oldChildren){
            childrenIndex[oldChild.dataset.nodeId] = oldChild
        }

        for (const child of sortedChildren) {
            container.append(childrenIndex[child.getId()])
        }
    }

    set_jsonata_helper(edit_field, hint_field, test_value, additional_function, isNode){
        // const edit_field = $(edit_field_selector)
        // const hint_field = $(hint_selector)
        // clean up so that multiple attribute edits don't accumulate        
        hint_field.empty()
        edit_field.off('input focus')
        edit_field.on('input focus', async (_) => {
            const current_value = edit_field.val()
            try{
                function undanger(text){
                    edit_field.removeClass('border border-danger')
                    hint_field.removeClass('text-danger')
                    hint_field.removeClass('text-warning')
                    hint_field.text(text ?? '')
                }

                if(!current_value){
                    undanger()
                    return
                }
                const expression = jsonata(current_value)
                let result
                if(isNode){
                    result = await this.performJsonataCall(expression, test_value)
                }else{
                    result = await expression.evaluate(test_value)
                }
                undanger(`Result based on current attribute value "${test_value}": ${result}. `)
                
                if(additional_function != undefined){
                    additional_function.call({
                        edit_field: edit_field,
                        hint_field: hint_field,
                        result: result,
                        attribute: test_value
                    })
                }
            }catch(e){
                console.error(e)
                edit_field.addClass('border border-danger')
                hint_field.addClass('text-danger')
                hint_field.text(`Error with jsonata expression: ${e.message}`)
            }
        })
    }

    buildNodeDict(node){
        node.traverseTree((node) => {
            this.node_dict[node.getId()] = node
        })
    }

    async addNode(node, parent_container, depth, cloneDepth, clonedId) {
        if(depth > 32) {
            this.addNode(new Node(
                'unknown', 
                '', 
                'Unknown', 
                'Depth limit reached',
                []
            ), parent_container, 0, 1)
            return
        }

        if(node.getClonedId()) {
            const clonedNode = this.node_dict[node.getClonedId()]
            if(!clonedNode) {
                this.addNode(new Node(
                    'unknown', 
                    node.getParentPath(), 
                    'Unknown', 
                    'Original node inaccessible or deleted',
                    []
                ), parent_container, depth + 1, 1)
                return
            }
            clonedNode.cloneCount = (clonedNode.cloneCount ?? 0) + 1
            this.addNode(clonedNode, parent_container, depth + 1, 0, node.getId())
            return
        }

        const node_div = this.node_template.clone()
        let node_id = node.getId()
        
        node_div.attr('data-node-id', node_id)

        const idText = $('#node-id', node_div)
        idText.click(event => event.stopPropagation())
        if (cloneDepth == undefined){
            node_div.attr('id', `node-${node_id}`)
            idText.text(node_id)
            $('#node-name', node_div).text(node.getName())
        }else{
            node_div.attr('id', `node-${node_id}-clone`)
            node_div.attr('data-clone-depth', cloneDepth)
            node_div.attr('data-clone-id', clonedId)
            idText.text(`Clone of ${node_id}`)
            idText.click(() => {
                this.focusNode(node_id, false)
                this.animateNode(node_id, false)
            })
            $('#node-name', node_div).html(`${node.getName()} <span class="fw-lighter fst-italic">Clone</span>`)
        }

        let parentNamePath = 'Unkown cloned original path'
        const parentIds = node.getParentPath().substring(1).split('/')
        parentNamePath = parentIds
                                .map(nodeId => this.node_dict[nodeId])
                                .filter(node => node)
                                .map(node => node.getName())
                                .join('/')
        idText.tooltip({
            title: `Parent located under "${parentNamePath}"`
        })

        $('#node-description', node_div).text(node.getDescription())

        // const button_children = $('#button-toggle-children', node_div)

        const child_container = $('#node-children', node_div)

        node_div.click(event => {
            // ignore if user marked text instead of clicking
            if(getSelection().toString()) return
            if(event.target.nodeName == 'INPUT') return
            child_container.collapse('toggle')
            event.stopPropagation()
        })


        const attributes_container = $('#attributes-container', node_div)
        attributes_container.empty()

        const container = $('.attribute-container', node_div)

        const attributes = (node.attributes ?? [])

        const attributes_hidden = attributes.filter(attribute => (attribute.ui?.hide))

        const button_show_hidden = $('#button-show-hidden', node_div)

        button_show_hidden.toggle(attributes_hidden.length > 0)

        button_show_hidden.click(event => {
            $('.attribute-row', node_div).show()
            button_show_hidden.hide()
            event.stopPropagation()
        })

        parent_container.append(node_div)

        container.toggle(attributes.length > attributes_hidden.length)
        if (attributes.length > 0) {
            if(node.attributesTemplate != undefined){
                const comment = $('#attributes-copy-comment', container)
                comment.show()
                comment.click(_ => this.animateNode(node.attributesTemplate))
            }
            let node_valid = true
            for (const attribute of attributes) {
                const row = this.attribute_row_template.clone()
                row.attr('data-attribute-key', attribute.id)

                node_valid &= attribute.valid ?? true

                const div_name = $('#attribute-name', row)
                div_name.text(attribute.label)

                const handleValueChange = async (event) => {
                    const target = event.currentTarget
                    let value = target.value
                    if(target.type == 'checkbox'){
                        value = target.checked
                    }
                    this.onnodeattributevalueupdate(node_id, attribute.id, value)
                }

                const stopPropagation = event => event.stopPropagation()

                const value = attribute.value_display ?? attribute.value
                let div_value = $('#attribute-value', row)
                div_value.attr('data-value-type', attribute.value_type);

                const edit = $('input.attribute-value-edit', row)
                edit.hide()

                const attribute_write_allowed = (this.permission_attributes_write && !(attribute.ui?.read_only))

                if(attribute.value_type == 'boolean'){
                    // we are adding this class instead of calling hide()
                    // since we never want to override #attribute-value.show()
                    div_value.addClass('d-none')
                    let checkbox = $('#attribute-value-checkbox', row)
                    checkbox.show()
                    checkbox = $('input', checkbox)
                    checkbox.prop('disabled', !attribute_write_allowed)
                    checkbox.change(handleValueChange.bind(this))
                }else{
                    if(attribute_write_allowed){
                        function restoreLayout(){
                            $('.attribute-value-edit').hide()
                            $('.attribute-value').show()
                        }
                        div_value.click((event) => {
                            event.stopPropagation()
                            restoreLayout()
                            edit.val(attribute.value)
                            edit.show()
                            edit.focus()
                            div_value.hide()
                            edit.on('focusout', restoreLayout)
                        })
                        edit.change(async (event) => {
                            restoreLayout()
                            await handleValueChange(event)
                        })
                    }
                }

                (function(){
                    if(!attribute_write_allowed) return
                    
                    if(attribute.value_type != 'integer') return

                    edit.attr('type', 'number')

                    if(attribute.range == undefined) return
                    if(typeof(attribute.range.min) != 'number') return
                    if(typeof(attribute.range.max) != 'number') return
                    const range = attribute.range
                    if(range.min == 0 && range.min == range.max) return
                    let slider = $('#attribute-value-slider', row)
                    slider.show()
                    // slider = $('input', slider)
                    const combined = $.merge(slider, edit)
                    combined.attr('min', attribute.range.min)
                    combined.attr('max', attribute.range.max)
                    
                    slider.on('input', _ => div_value.text(slider.val()))
                    slider.change(handleValueChange)
                })()

                if(attribute.ui?.hide) {
                    // adding it anyway so we can show it later
                    row.hide()
                    div_name.addClass('text-muted')
                }

                attributes_container.append(row)

                await this.setNodeAttribute(node_id, attribute, false, false)
            }

            this.setNodeValidColor(node_id, node_valid)
        }

        node_div.show()

        // if (node.children.length < 1) {
        //     button_children.prop('disabled', true)
        // }

        if(cloneDepth != undefined) {
            cloneDepth++
        }
        for (const child of node.children) {
            await this.addNode(child, child_container, depth + 1, cloneDepth)
        }
    }

    element_animate(selector, animation_class, duration){
        const element = $(selector)
        element.css({
            animation: `${animation_class} ${duration}ms`
        })
        setTimeout(_ => {
            element.css({animation: ''})
        }, duration)
    }

    animateNode(node_id, includeClones=true){
        let selector = `div[data-node-id="${node_id}"] > div#node-without-children`
        if(!includeClones) {
            selector = `div[data-node-id="${node_id}"]:not([data-clone-depth]) > div#node-without-children`
        }
        this.element_animate(selector, 'notification-grey', 500)
    }

    animateAttribute(node_id, attribude_id, valid){
        const animation_name = valid ? 'notification-green' : 'notification-red'
        this.element_animate(`div[data-node-id="${node_id}"] > div > div#attribute-container div[data-attribute-key="${attribude_id}"]`, animation_name, 500)
    }

    async focusNode(node_id, animate = true) {
        const node = this.node_dict[node_id]
        const parentIds = node.getParentPath().split('/').slice(1)

        for (const nodeIdToExpand of parentIds) {            
            const container = $(`div[data-node-id="${nodeIdToExpand}"]:not([data-clone-depth]) > div#node-children`)
            if(container.length === 0) {
                // not found, probably outside our scope
                continue
            }
            if(container.hasClass('show')) {
                // already expanded
                continue
            }
            if(!animate) {
                container.addClass('show')
                continue
            }
            await new Promise((resolve) => {
                container.on('shown.bs.collapse', () => {
                    container.off('shown.bs.collapse')
                    resolve()
                })
                
                container.collapse('show')
            })
        }


        document.querySelector(`#node-${node_id}`).scrollIntoView()
    }

    setNodeValidColor(node_id, valid) {
        const container = $(`div[data-node-id="${node_id}"]`)
        if (valid) {
            container.removeClass('node-error')
        } else {
            container.addClass('node-error')
        }
    }

    async setNodeAttribute(node_id, attribute, update_in_place, apply_sorting) {
        const node = this.node_dict[node_id]
        if(update_in_place && node != undefined){
            for(const old_attribute of node.attributes ?? []){
                if(old_attribute.id == attribute.id){
                    // should we break here?
                    // if we disallow multiple attributes with the same id, the yes.
                    // have we decided that already? nope...
                    old_attribute.value = attribute.value
                }
            }
        }

        const attribute_key = attribute.id
        const valid = attribute.valid ?? true
        const attribute_value = attribute.value_display ?? attribute.value
        const container = $(`div[data-node-id="${node_id}"] > div > div#attribute-container div.attribute-row[data-attribute-key="${attribute_key}"]`)
        const div_value = $(`.attribute-value`, container)

        const dateUpload = new Date(attribute.timestamp_upload * 1000).toLocaleString()
        const dateChanged = new Date(attribute.timestamp_change * 1000).toLocaleString()

        let toolbox_anchor = div_value

        if(div_value.data('valueType') == 'boolean'){
            toolbox_anchor = $('div#attribute-value-checkbox', container)
            const checkbox = $('input', toolbox_anchor)
            checkbox.prop('checked', attribute_value)
            checkbox.toggleClass('text-danger', !valid)
        }else{
            div_value.text(attribute_value ?? '-')
            div_value.toggleClass('attribute-value-undefined', attribute_value == undefined)
            div_value.toggleClass('text-danger', !valid)
        }

        if(div_value.data('valueType') == 'integer'){
            $('#attribute-value-slider', container).val(attribute.value)
        }

        toolbox_anchor.tooltip('dispose')
        toolbox_anchor.tooltip({
            title: `Raw value: ${attribute.value}\nTimestamp (upload): ${dateUpload}\nTimestamp (value change): ${dateChanged}`
        })

        if(apply_sorting){
            await this.sortNodeChildren(this.rootNode)
            await this.filterNodeChildren(this.rootNode)
        }
    }

    getNodeParent(node){
        if(!node.getParentPath()){
            return undefined
        }
        const parts = node.getParentPath().split('/')
        return this.node_dict[parts[parts.length - 1]]
    }

    getExpandedNodeIds() {
        const expandedNodes = $('.node-container:has("> div#node-children.collapse.show"), .node-container:has("> div#node-children.collapsing")')
        return expandedNodes
            .map(function () { return $(this).attr('data-node-id') })
            .toArray()
    }

    setExpandedNodeIds(expandedNodeIds) {
        for (const expandedNodeId of expandedNodeIds) {
            $(`div[data-node-id="${expandedNodeId}"] > #node-children`).addClass('show')
        }
    }

    expandNode(nodeId) {
        const expanded = this.getExpandedNodeIds()
        if(expanded.includes(nodeId)) {
            return
        }

        expanded.push(nodeId)
        this.setExpandedNodeIds(expanded)
    }

    getNodeFromDiv(div){
        return this.node_dict[div.data('nodeId')]
    }

    attachContextMenu(parentContainer){

        function copyNode(option, event){
            this.copied_node = this.getNodeFromDiv(event.$trigger)
            this.node_is_cut = (option == 'cut')
        }

        async function deleteNode(_, event){
            console.log({_, event})
            const dialog = $('#dialog-node-delete')
            const cloneDepth = event.$trigger.attr('data-clone-depth')
            let node = this.getNodeFromDiv(
                event.$trigger
            )
            if(cloneDepth === '0') {
                node = this.node_dict[event.$trigger.attr('data-clone-id')]
                $('#clone-warning', dialog).text('This will only delete the clone, not the original')
            }else if(cloneDepth !== undefined) {
                $('#clone-warning', dialog).text('THIS WILL DELETE THE ORIGINAL!')
            }
            $('#node-name', dialog).text(node.name)
            var deletionModal = new bootstrap.Modal(dialog)
            deletionModal.show()
            const confirmButton = $('#confirm', dialog)
            confirmButton.off('click')
            confirmButton.click(
                this.runWithDialog.bind(this, this.onnodeupdate, 'delete', node)
            )
        }

        async function nodeAccounts(_, event){
            await this.showAccountsDialog(
                this.getNodeFromDiv(
                    event.$trigger
                )
            )
        }

        function expandNode(_, event){
            const expandedIds = []
            const node = this.getNodeFromDiv(event.$trigger)
            function expand(node){
                expandedIds.push(node.getId())
                for(const child of node.children ?? []){
                    expand(child)
                }
            }
            expand(node)
            this.setExpandedNodeIds(expandedIds)
        }

        async function pasteNodeTemplate(_, event){
            await this.node_paste_template(
                this.getNodeFromDiv(
                    event.$trigger
                )
            )
        }
        
        async function pasteNodeCopy(_, event){
            await this.node_paste_deep_copy(
                this.getNodeFromDiv(
                    event.$trigger
                )
            )
        }
        
        async function pasteNodeClone(_, event){
            await this.node_paste_clone(
                this.getNodeFromDiv(
                    event.$trigger
                )
            )
        }

        async function showNodeDialog(menu_key, event) {
            const node = this.getNodeFromDiv(
                event.$trigger
            )
            const node_id = node.getId()
            const dialog = $('#dialog-edit-node')

            $('#node-edit-attributes-container', dialog).toggle(node.attributesTemplate == undefined)

            const button_done = $('#button-node-edit-done', dialog)
            const button_delete = $('#button-node-edit-delete', dialog)

            button_done.off('click')
            button_delete.off('click')

            const is_edit = menu_key == 'edit'

            const input_name = $('#input-node-name', dialog)
            const node_description = $('#input-node-description', dialog)
            const sortInput = $('#input-node-sort', dialog)
            const filterInput = $('#input-node-filter', dialog)

            let title = 'Edit node'

            let attributes = node.attributes ??= []

            if (is_edit) {
                input_name.val(node.getName())
                node_description.val(node.getDescription())
                sortInput.val(node.getSortExpression())
                filterInput.val(node.getFilterExpression())
            } else {
                input_name.val('')
                node_description.val('')
                sortInput.val('')
                filterInput.val('')

                title = 'Create node'
                attributes = []
            }

            const firstChild = (node.children ?? [])[0]

            function setOkText(){
                if(firstChild){
                    this.hint_field.text(`Result based on first child: ${this.result}`)
                }else{
                    this.hint_field.text('Expressions seems good! Can\'t properly test without children...')
                }
            }

            this.set_jsonata_helper(sortInput, $('#hint-sort', dialog), firstChild ?? {attributes: []}, setOkText, true)
            this.set_jsonata_helper(filterInput, $('#hint-filter', dialog), firstChild ?? {attributes: []}, setOkText, true)

            dialog.dialog({
                title: title,
                buttons: [
                    {
                        text: is_edit ? 'Edit' : 'Create',
                        click: async (...args) => {
                            dialog.dialog('close')
                            const newNode = new Node(
                                undefined,
                                undefined,
                                input_name.val(),
                                node_description.val(),
                                attributes,
                            )
                            newNode.setSortExpression(sortInput.val())
                            newNode.setFilterExpression(filterInput.val())
                            if (is_edit) {
                                newNode.setId(node_id)
                            } else {
                                newNode.setParentPath(`${node.getParentPath()}/${node_id}`)

                                const expandedNodeIds = this.getExpandedNodeIds()
                                if(!expandedNodeIds.includes(node_id)){
                                    this.setExpandedNodeIds(expandedNodeIds.concat(node_id))
                                }
                            }
                            await this.runWithDialog(this.onnodeupdate, is_edit ? 'edit' : 'create', newNode)
                        },
                        show: false,
                        class: 'btn btn-primary'
                    }
                ]
            })
            
            const attributeContainer = $('#container-attributes', dialog)
            const attributeSample = $('#attribute-template').contents()
            const buttonPlus = $('#button-attribute-add', dialog)
            const editId = $('input#attribute-id')
            const editLabel = $('input#attribute-name')
            buttonPlus.off('click')
            async function attributeEditHandler(attribute) {
                const isEdit = !!attribute
                console.log({isEdit, attribute})
                const attributeValue = attribute?.value
                const handler_map = {
                    filter: function(editField, hintField){
                        this.set_jsonata_helper(editField, hintField, attributeValue, function(){
                            if(typeof(this.result) != 'boolean'){
                                this.hint_field.addClass('text-warning')
                                this.hint_field.append('Result value should be a boolean')
                                return
                            }
                            this.hint_field.append(`The current value would be ${this.result ? "accepted" : "rejected"} by the API.`)
                        })
                    },
                    validation: function(editField, hintField){
                        this.set_jsonata_helper(editField, hintField, attributeValue, function(){
                            if(typeof(this.result) != 'boolean'){
                                this.hint_field.addClass('text-warning')
                                this.hint_field.append('Result value should be a boolean')
                                return
                            }
                            this.hint_field.append(`The current value would be ${this.result ? "green" : "red"} in the user interface.`)
                        })
                    },
                    transformation: function(editField, hintField){
                        this.set_jsonata_helper(editField, hintField, attributeValue, function(){
                            const allowed_types = ['string', 'number', 'boolean']
                            const actual_type = typeof(this.result)
                            if(!allowed_types.includes(actual_type)){
                                this.hint_field.addClass('text-warning')
                                this.hint_field.append(`Result value type must be one of [${allowed_types}], is ${actual_type}`)
                                return
                            }
                            this.hint_field.append(`The value ${this.result} (type: ${actual_type}) will be written to the database.`)
                        })
                    },
                    transformation_display: function(editField, hintField){
                        this.set_jsonata_helper(editField, hintField, attributeValue, function(){
                            this.hint_field.append(`"${this.result}" will be shown in the user interface.`)
                        })
                    },
                }

                $('#attribute-field-container').empty()
                function addEditField(category, contents){
                    const edit = $('#template-attribute-field').contents().clone()
                    $(`div[data-attribute-field-category="${category}"]`).show()
                    $(`div[data-attribute-field-category="${category}"] > div#attribute-field-container`).append(edit)
                    $('input', edit).val(contents)
                    return edit
                }
                $(`div[data-attribute-field-category]`).hide()
                $(`div[data-attribute-field-category] > div#attribute-field-container`).empty()
                $('a[data-attribute-field-category]').off('click')
                $('a[data-attribute-field-category]').click(event => {
                    const category = event.currentTarget.dataset.attributeFieldCategory
                    const fieldContainer = addEditField(category)
                    const edit = $('input', fieldContainer)
                    edit.focus()
                    const handler = handler_map[category]
                    if(handler != undefined){
                        handler.call(this, edit, $('#hint-filter', fieldContainer))
                    }
                    this.attachTooltips()
                })

                if(isEdit){
                    for(const category of ['filter', 'validation', 'transformation', 'transformation_display', 'custom']){
                        for(let field of (attribute[category] ?? [])){
                            if(category != 'custom'){
                                field = field.jsonata
                            }
                            addEditField(category, field)
                        }
                    }
                    this.attachTooltips()
                }

                this.editObjectDialog('#dialog-attribute-edit', attribute)
                    .then(edited => {
                        for(const fieldContainier of $('div[data-attribute-field-category]')){
                            const category = fieldContainier.dataset.attributeFieldCategory
                            edited[category] = 
                                $('input', fieldContainier)
                                    .toArray()
                                    .map(edit => edit.value)
                                    .filter(value => (value && value.trim()))
                                    .map(value => category != 'custom' ? {jsonata: value} : value)
                        }
                        if (isEdit) {
                            Object.assign(attribute, edited)
                        } else {
                            attributes.push(edited)
                        }
                        loadAttributes()
                    })
                $('#dialog-attribute-edit #attribute-id').prop('disabled', isEdit)
                
                function toggleRange(){
                    $('#dialog-attribute-edit #attribute-range').toggle(
                        $('#dialog-attribute-edit #attribute-type').val() == 'integer'
                    )
                }
                $('#dialog-attribute-edit #attribute-type').change(toggleRange)
                toggleRange()
            }
            

            const loadAttributes = () => {
                attributeContainer.empty()
                for (const attributeIndex in attributes) {
                    const attribute = attributes[attributeIndex]
                    const container = attributeSample.clone()
                    $('#attribute-name', container).text(attribute.label)
                    $('#attribute-id', container).text(attribute.id)
                    $('#btn-edit', container).click(attributeEditHandler.bind(this, attribute))
                    function spliceAndMove(distance){
                        return function(){
                            attributes.splice(attributeIndex, 1)
                            if(distance){
                                attributes.splice(Number(attributeIndex) + distance, 0, attribute)
                            }
                            loadAttributes()
                        }
                    }
                    $('#btn-delete', container).click(spliceAndMove())
                    if(attributeIndex > 0){
                        $('#btn-up', container).click(spliceAndMove(-1))
                    }
                    if(attributeIndex < (attributes.length - 1)){
                        $('#btn-down', container).click(spliceAndMove(+1))
                    }
                    attributeContainer.append(container)
                }
            }

            buttonPlus.click(attributeEditHandler.bind(this, undefined))
            loadAttributes()
        }

        parentContainer.contextMenu({
            selector: `div.node-container`,
            items: {
                edit: {
                    name: 'Edit',
                    icon: 'edit',
                    callback: showNodeDialog.bind(this),
                    disabled: () => !this.permission_node_write
                },
                delete: {
                    name: 'Delete',
                    icon: 'delete',
                    callback: deleteNode.bind(this),
                    disabled: () => !this.permission_node_write,
                },
                copy: {
                    name: 'Copy',
                    icon: 'copy',
                    callback: copyNode.bind(this),
                    disabled: () => !(this.permission_node_write && this.permission_children)
                },
                cut: {
                    name: 'Cut',
                    icon: 'cut',
                    callback: copyNode.bind(this),
                    disabled: () => !(this.permission_node_write && this.permission_children)
                },
                paste: {
                    name: 'Paste',
                    icon: 'paste',
                    disabled: () => this.copied_node == null,
                    items: {
                        paste_deep_copy: {
                            name: 'as copy',
                            callback: pasteNodeCopy.bind(this)
                        },
                        paste_template: {
                            name: 'as template',
                            callback: pasteNodeTemplate.bind(this),
                            disabled: () => this.node_is_cut
                        },
                        paste_clone: {
                            name: 'as clone',
                            callback: pasteNodeClone.bind(this),
                            disabled: () => this.node_is_cut
                        },
                    },
                },
                child_add: {
                    name: 'Add child',
                    icon: 'add',
                    callback: showNodeDialog.bind(this),
                    disabled: () => !(this.permission_node_write && this.permission_children)
                },
                accounts: {
                    name: 'Manage accounts / API keys',
                    icon: 'ui-icon-key',
                    callback: nodeAccounts.bind(this),
                    disabled: () => !this.permission_accounts
                },
                expand: {
                    name: 'Expand all',
                    callback: expandNode.bind(this)
                }
            }
        })
    }

    async setRootNode(node) {
        this.rootNode = node
        this.node_dict = {}
        this.parent_container.empty()
        this.buildNodeDict(node)
        await this.addNode(node, this.parent_container, 0)
        await this.sortNodeChildren(node)
        await this.filterNodeChildren(node)
    }
}
